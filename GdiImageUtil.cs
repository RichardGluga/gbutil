﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;

namespace gb.util {
    public class GdiImageUtil {
        public static Image resize_and_skew(Image srcImage, int newWidth, int newHeight) {
            Bitmap newImage = new Bitmap(newWidth, newHeight);
            using (Graphics gr = Graphics.FromImage(newImage)) {
                gr.SmoothingMode = SmoothingMode.HighQuality;
                gr.InterpolationMode = InterpolationMode.HighQualityBicubic;
                gr.PixelOffsetMode = PixelOffsetMode.HighQuality;
                gr.DrawImage(srcImage, new Rectangle(0, 0, newWidth, newHeight));
            }

            return (Image)newImage;
        }

        public static Image resize_maintain_aspect_max(Image srcImage, int maxWidth, int maxHeight) {
            Image resized = srcImage;

            int wDelta = srcImage.Width - maxWidth;
            int hDelta = srcImage.Height - maxHeight;
            bool horizontal = srcImage.Width / srcImage.Height > 1;

            //source image is same or smaller than max dimensions, no resample needed
            if(wDelta <= 0 && hDelta <= 0) return srcImage;

            int newHeight = maxWidth * srcImage.Height / srcImage.Width;
            int newWidth = maxHeight * srcImage.Width / srcImage.Height;
            if (newHeight <= maxHeight) resized = resize_and_skew(srcImage, maxWidth, newHeight);
            else resized = resize_and_skew(srcImage, newWidth, maxHeight);

            return resized;
        }

        public static Image resize_maintain_aspect_min(Image srcImage, int minWidth, int minHeight) {
            Image resized = srcImage;

            int wDelta = srcImage.Width - minWidth;
            int hDelta = srcImage.Height - minHeight;
            bool horizontal = srcImage.Width / srcImage.Height > 1;

            //source image is same or smaller than max dimensions, no resample needed
            if (wDelta <= 0 && hDelta <= 0) return srcImage;

            int newHeight = minWidth * srcImage.Height / srcImage.Width;
            int newWidth = minHeight * srcImage.Width / srcImage.Height;
            if (newHeight <= minHeight) resize_and_skew(srcImage, newWidth, minHeight);
            else resized = resize_and_skew(srcImage, minWidth, newHeight);

            return resized;
        }

        public static Image resize_and_crop(Image srcImage, int newWidth, int newHeight)
        {
            Image resized = resize_maintain_aspect_min(srcImage, newWidth, newHeight);

            int wDelta = resized.Width - newWidth;
            int hDelta = resized.Height - newHeight;

            //source image is same or smaller than max dimensions, no crop needed
            if (wDelta <= 0 && hDelta <= 0) return resized;

            int fromX = 0;
            int fromY = 0;
            int toX = resized.Width;
            int toY = resized.Height;
            
            //first crop height
            if (hDelta > 0) {
                fromY = hDelta / 2;
                toY = fromY+newHeight;
            }
            //next crop width
            if (wDelta > 0) {
                fromX = wDelta / 2;
                toX = toX + newWidth;
            }

            //do the crop
            Bitmap newImage = new Bitmap(toX-fromX, toY-fromY);
            using(Graphics gr = Graphics.FromImage(newImage)) {
                gr.SmoothingMode = SmoothingMode.HighQuality;
                gr.InterpolationMode = InterpolationMode.HighQualityBicubic;
                gr.PixelOffsetMode = PixelOffsetMode.HighQuality;
                gr.DrawImage(resized, new Rectangle(0, 0, toX-fromX, toY-fromY), new Rectangle(fromX, fromY, toX-fromX, toY-fromY), GraphicsUnit.Pixel);
            }

            return (Image) newImage;
        }

        public static void saveImage(Image image, string filename, ImageCodecInfo encoder, int quality) {
            EncoderParameters myEncoderParameters = new EncoderParameters();
            myEncoderParameters.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, quality);
            image.Save(filename, encoder, myEncoderParameters);
        }

        public static void saveImage(Image image, string filename, string extension, int quality) {
            ImageCodecInfo encoder = GdiImageUtil.GetEncoderByMimeType("image/" + extension);
            saveImage(image, filename, encoder, quality);
        }

        public static ImageCodecInfo GetEncoder(ImageFormat format) {
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();
            foreach (ImageCodecInfo codec in codecs) {
                if (codec.FormatID == format.Guid) {
                    return codec;
                }
            }
            return null;
        }

        public static ImageCodecInfo GetEncoderByExtension(String ext) {
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();
            foreach (ImageCodecInfo codec in codecs) {
                if (codec.FilenameExtension == ext) {
                    return codec;
                }
            }
            return null;
        }

        public static ImageCodecInfo GetEncoderByMimeType(String mime) {
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();
            foreach (ImageCodecInfo codec in codecs) {
                if (codec.MimeType == mime) {
                    return codec;
                }
            }
            return null;
        }

        public static ImageCodecInfo GetDecoder(ImageFormat format) {
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();
            foreach (ImageCodecInfo codec in codecs) {
                if (codec.FormatID == format.Guid) {
                    return codec;
                }
            }
            return null;
        }
    }
}
