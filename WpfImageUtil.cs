﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;



namespace GBUtils {
    public class WpfImageUtil {
        public static System.Drawing.Point getImageDimensions(string filename) {
            Stream imageStreamSource = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read);
            BitmapDecoder decoder = BitmapDecoder.Create (imageStreamSource, BitmapCreateOptions.None, BitmapCacheOption.None);
            BitmapSource bitmapSource = decoder.Frames[0];

            System.Drawing.Point p = new System.Drawing.Point(bitmapSource.PixelWidth, bitmapSource.PixelHeight);
            imageStreamSource.Close();
            return p;
        }

        public static BitmapSource loadBitmapSource(string filename) {
            var photoDecoder = BitmapDecoder.Create(
                new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read),
                BitmapCreateOptions.PreservePixelFormat,
                BitmapCacheOption.None);
            var photo = photoDecoder.Frames[0];
            return photo;
        }

        ////http://weblogs.asp.net/bleroy/archive/2009/12/10/resizing-images-from-the-server-using-wpf-wic-instead-of-gdi.aspx
        public static BitmapFrame resize_and_skew(BitmapSource photo, int width, int height) {
            try {
                DrawingGroup group = new DrawingGroup(); 
                RenderOptions.SetBitmapScalingMode(group, BitmapScalingMode.Fant);
                group.Children.Add(new ImageDrawing(photo, new System.Windows.Rect(0, 0, width, height)));
                DrawingVisual targetVisual = new DrawingVisual();
                var targetContext = targetVisual.RenderOpen();
                targetContext.DrawDrawing(group);
                RenderTargetBitmap target = new RenderTargetBitmap(width, height, 96, 96, PixelFormats.Default); 
                targetContext.Close();
                target.Render(targetVisual);
                BitmapFrame targetFrame = BitmapFrame.Create(target);
                return targetFrame;
            } catch (Exception ex) {
                var a = 1;
            }
            return null;
        }

        private System.Drawing.Image imageFromBitmapFrame(BitmapSource bitmapsource) {
            System.Drawing.Bitmap bitmap;
            using (MemoryStream outStream = new MemoryStream()) {
                BitmapEncoder enc = new BmpBitmapEncoder();
                enc.Frames.Add(BitmapFrame.Create(bitmapsource));
                enc.Save(outStream);
                bitmap = new System.Drawing.Bitmap(outStream);
            }
            return bitmap;
        }

        public static BitmapSource resize_maintain_aspect_max(BitmapSource srcImage, int maxWidth, int maxHeight) {
            BitmapSource resized = srcImage;

            int wDelta = srcImage.PixelWidth - maxWidth;
            int hDelta = srcImage.PixelHeight - maxHeight;
            bool horizontal = srcImage.Width / srcImage.Height > 1;

            //source image is same or smaller than max dimensions, no resample needed
            if (wDelta <= 0 && hDelta <= 0) return srcImage;

            int newHeight = maxWidth * srcImage.PixelHeight / srcImage.PixelWidth;
            int newWidth = maxHeight * srcImage.PixelWidth / srcImage.PixelHeight;
            if (newHeight <= maxHeight) resized = resize_and_skew(srcImage, maxWidth, newHeight);
            else resized = resize_and_skew(srcImage, newWidth, maxHeight);

            return resized;
        }

        public static BitmapSource resize_maintain_aspect_min(BitmapSource srcImage, int minWidth, int minHeight) {
            BitmapSource resized = srcImage;

            int wDelta = srcImage.PixelWidth - minWidth;
            int hDelta = srcImage.PixelHeight - minHeight;
            bool horizontal = srcImage.Width / srcImage.Height > 1;

            //source image is same or smaller than max dimensions, no resample needed
            if (wDelta <= 0 && hDelta <= 0) return srcImage;

            int newHeight = minWidth * srcImage.PixelHeight / srcImage.PixelWidth;
            int newWidth = minHeight * srcImage.PixelWidth / srcImage.PixelHeight;
            if (newHeight <= minHeight) resize_and_skew(srcImage, newWidth, minHeight);
            else resized = resize_and_skew(srcImage, minWidth, newHeight);

            return resized;
        }

        public static BitmapSource resize_and_crop(BitmapSource srcImage, int newWidth, int newHeight) {
            BitmapSource resized = resize_maintain_aspect_min(srcImage, newWidth, newHeight);

            int wDelta = resized.PixelWidth - newWidth;
            int hDelta = resized.PixelHeight - newHeight;

            //source image is same or smaller than max dimensions, no crop needed
            if (wDelta <= 0 && hDelta <= 0) return resized;

            int fromX = 0;
            int fromY = 0;
            int toX = resized.PixelWidth;
            int toY = resized.PixelHeight;

            //first crop height
            if (hDelta > 0) {
                fromY = hDelta / 2;
                toY = fromY + newHeight;
            }
            //next crop width
            if (wDelta > 0) {
                fromX = wDelta / 2;
                toX = toX + newWidth;
            }

            var group = new DrawingGroup();
            RenderOptions.SetBitmapScalingMode(group, BitmapScalingMode.Fant);
            group.Children.Add(new ImageDrawing(srcImage, new System.Windows.Rect(fromX, fromY, toX - fromX, toY - fromY)));
            var targetVisual = new DrawingVisual();
            var targetContext = targetVisual.RenderOpen();
            targetContext.DrawDrawing(group);
            var target = new RenderTargetBitmap(toX - fromX, toY - fromY, 96, 96, PixelFormats.Default);
            targetContext.Close();
            target.Render(targetVisual);
            var targetFrame = BitmapFrame.Create(target);

            return targetFrame;
        }

        public static void saveImage(BitmapSource image, string filename, BitmapEncoder encoder, int quality) {
            using (var fileStream = new FileStream(filename, FileMode.Create)) {
                encoder.Frames.Add(BitmapFrame.Create(image));
                encoder.Save(fileStream);
            }
        }

        public static void saveImage(BitmapSource image, string filename, string extension, int quality) {
            BitmapEncoder encoder = WpfImageUtil.GetEncoderByExt(extension);
            saveImage(image, filename, encoder, quality);
        }

        public static BitmapEncoder GetEncoderByExt(string extension) {
            BitmapEncoder encoder = null;
            if (extension.IndexOf(".") != 0) extension = "." + extension;

            switch (extension.ToLower()) {
                case ".jpg":
                case ".jpeg":
                    encoder = new JpegBitmapEncoder();
                    break;
                case ".bmp":
                    encoder = new BmpBitmapEncoder();
                    break;
                case ".png":
                    encoder = new PngBitmapEncoder();
                    break;
                case ".tif":
                case ".tiff":
                    encoder = new TiffBitmapEncoder();
                    break;
                case ".gif":
                    encoder = new GifBitmapEncoder();
                    break;
                case ".wmp":
                    encoder = new WmpBitmapEncoder();
                    break;
            }
            return encoder;
        }

        public static bool hasDecoderByExt(string extension) {
            if (extension.IndexOf(".") != 0) extension = "." + extension;

            bool decoder = false;
            switch (extension.ToLower()) {
                case ".jpg":
                case ".jpeg":
                    decoder = true;
                    break;
                case ".ico":
                case ".icon":
                    decoder = true;
                    break;
                case ".bmp":
                    decoder = true;
                    break;
                case ".png":
                    decoder = true;
                    break;
                case ".tif":
                case ".tiff":
                    decoder = true;
                    break;
                case ".gif":
                    decoder = true;
                    break;
                case ".wmp":
                    decoder = true;
                    break;
            }

            return decoder;
        }

        public static string[] listEncoderExtensions() {
            return new string[] { "bmp", "gif", "jpeg", "png", "tif", "wmp"};
        }
    }
}
